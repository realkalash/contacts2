package com.example.contacts2;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterListContact extends RecyclerView.Adapter<AdapterListContact.ViewHolder> {


    Context context;
    List<User> usersList;
    private OnItemClickListener listener;

    public AdapterListContact(Context context, List<User> usersList, OnItemClickListener listener) {
        this.context = context;
        this.usersList = usersList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int pos) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_contact, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int pos) {
        viewHolder.bind(usersList.get(pos), listener);
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    interface OnItemClickListener {
        void onItemClick(User user);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.rc_name)
        TextView name;
        @BindView(R.id.rc_email)
        TextView email;
        @BindView(R.id.rc_photocontact)
        ImageView photoContact;

        TextView adress;
        TextView phone;


        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final User user, final OnItemClickListener listener) {
            name.setText(user.getName());
            email.setText(user.getEmail());
            photoContact.setImageResource(user.getPhotoContact());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(user);
                }
            });
        }
    }
}
