package com.example.contacts2;

import android.text.Editable;

class User {
    private String name;
    private String email;
    private String phone;
    private String adress;
    private int photoContact;

    public User(String name, String adress, String email, String phone, int photoContact) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.adress = adress;
        this.photoContact = photoContact;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public int getPhotoContact() {
        return photoContact;
    }

    public void setPhotoContact(int photoContact) {
        this.photoContact = photoContact;
    }
}
