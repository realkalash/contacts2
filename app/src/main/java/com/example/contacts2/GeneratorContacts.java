package com.example.contacts2;

import java.util.ArrayList;
import java.util.List;

public class GeneratorContacts {

    List<User> userList = new ArrayList<>();

    public List<User> generateListUsers() {

        userList.add(new User("Alex Kalasnikov", "Nutona 134", "1realkalash@gmail.com", "+380993897262", R.drawable.image_alex));
        userList.add(new User("Vlad", "Holodna gora", "growmeup@gmail.com", "", R.drawable.image_vlad));
        userList.add(new User("Artem Nesterov", "", "", "", R.drawable.image_artem_nesterov));
        userList.add(new User("Viacheslav Gonchar", "", "smart.sourceit@gmail.com", "", R.drawable.image_viacheslav));
        userList.add(new User("Дмитрий Овсянников", "", "vertuback@gmail.com", "", R.drawable.image_dmitri_ovsiannikov));
        userList.add(new User("Изабелла Дударик", "", "", "", R.drawable.image_izabella_dudarik));
        userList.add(new User("Олександр Музыко", "", "", "", R.drawable.image_oleksand_musiko));
        userList.add(new User("Anonim", null, null, null, R.drawable.ic_launcher_foreground));

        return userList;
    }
}
