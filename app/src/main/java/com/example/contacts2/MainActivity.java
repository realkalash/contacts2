package com.example.contacts2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements AdapterListContact.OnItemClickListener {
    public static final String EXTRA_NAME = "EXTRA_NAME";
    public static final String EXTRA_ADRESS = "EXTRA_ADRESS";
    public static final String EXTRA_PHOTO = "EXTRA_PHOTO";
    @BindView(R.id.am_listContacts)
    RecyclerView listContacts;
    @BindView(R.id.fa_addContact)
    Button btnAddContact;

    GeneratorContacts generatorContacts = new GeneratorContacts();
    List<User> usersList = generatorContacts.generateListUsers();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        AdapterListContact adapterListContact;
        adapterListContact = new AdapterListContact(this,usersList, this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        listContacts.setLayoutManager(linearLayoutManager);

        listContacts.setAdapter(adapterListContact);
    }

    @Override
    public void onItemClick(User user) {
        Intent intent = new Intent(this, UserInfoActivity.class);

        intent.putExtra(EXTRA_NAME, user.getName());
        intent.putExtra(Intent.EXTRA_EMAIL, user.getEmail());
        intent.putExtra(EXTRA_ADRESS, user.getAdress());
        intent.putExtra(Intent.EXTRA_PHONE_NUMBER, user.getPhone());
        intent.putExtra(EXTRA_PHOTO, user.getPhotoContact());
        startActivity(intent);

    }

    @OnClick(R.id.fa_addContact)
    public void addContactClick(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater  = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.activity_user_info,null);

        final EditText txtName =dialogView.findViewById(R.id.ca_name);
        final EditText txtPhone =dialogView.findViewById(R.id.ca_phone);
        final EditText txtAdress =dialogView.findViewById(R.id.ca_adress);
        final EditText txtEmail =dialogView.findViewById(R.id.ca_email);


        final EditText editText = new EditText(this);

        builder.setTitle("Add contact")
                .setView(dialogView)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        User newUser = new User(
                                txtName.getText().toString(),
                                txtAdress.getText().toString(),
                                txtEmail.getText().toString(),
                                txtPhone.getText().toString(),
                                R.drawable.ic_launcher_foreground);

                        usersList.add(newUser);
                        Objects.requireNonNull(listContacts.getAdapter()).notifyDataSetChanged();

                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
