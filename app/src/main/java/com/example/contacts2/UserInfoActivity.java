package com.example.contacts2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.contacts2.MainActivity.EXTRA_ADRESS;
import static com.example.contacts2.MainActivity.EXTRA_NAME;
import static com.example.contacts2.MainActivity.EXTRA_PHOTO;

public class UserInfoActivity extends AppCompatActivity {

    @BindView(R.id.ca_name)
    EditText name;
    @BindView(R.id.ca_adress)
    EditText adress;
    @BindView(R.id.ca_email)
    EditText email;
    @BindView(R.id.ca_phone)
    EditText phone;
    @BindView(R.id.ca_photoContact)
    ImageView caPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        ButterKnife.bind(this);

        name.setText(String.format("Name: %s", getIntent().getStringExtra(EXTRA_NAME)));
        adress.setText(String.format("Adress: %s", getIntent().getStringExtra(EXTRA_ADRESS)));
        email.setText(String.format("Email: %s", getIntent().getStringExtra(Intent.EXTRA_EMAIL)));
        phone.setText(String.format("Phone number: %s", getIntent().getStringExtra(Intent.EXTRA_PHONE_NUMBER)));
        caPhoto.setImageResource(getIntent().getIntExtra(EXTRA_PHOTO, R.drawable.ic_launcher_foreground));
    }
}
